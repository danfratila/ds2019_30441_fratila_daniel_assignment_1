import { EventEmitter } from "events";
import RestPatient from "../rest/RestPatient";

        
class PatientModel extends EventEmitter {
    constructor() {
        super();
        this.restPatient = new RestPatient();
        this.state = {
            patients: [],
            newPatient: {
                firstname: "",
                lastname: "",
                birthdate: "",
                gender: "",
                address: "",
                record: ""
            },
            updatedPatient: {
                patientId: "",
                firstname: "",
                lastname: "",
                birthdate: "",
                gender: "",
                address: "",
                record: ""
            },
            filterTag : "",
            filteredPatients: [],
            deleteId: "",
            specificPatients: []
        };
    }

    loadPatients() {
        return this.restPatient.loadAllPatients().then(patients => {
            this.state = { 
                ...this.state,
                patients: patients
            };
            this.emit("change", this.state);
        })
    }


    // --------------------------------------------------------
    // add
    addPatient(firstName, lastName, birthdate, gender, address, record){
        return this.restPatient.createPatient(firstName, lastName, birthdate, gender, address, record)
            .then(patient => this.appendPatient(patient));
    }

    appendPatient(patient){
        this.state = {
            ...this.state,
            patients: this.state.patients.concat([patient])
        };
        this.emit("change", this.state);
    }

    changeNewPatientProperty(property, value) {
        this.state = {
            ...this.state,
            newPatient: {
                ...this.state.newPatient,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }


    // --------------------------------------------------------
    // filter
    filterByName(){
        return this.restPatient.loadCertainPatientByName(this.state.filterTag)
            .then(patients => {
                this.state = {
                    ...this.state,
                    filteredPatients: patients
                };
                this.emit("change", this.state);
            });
    }

    changeFilterProperty(value){
        this.state = {
            ...this.state,
            filterTag: value
        };
        this.emit("change", this.state);
    }

    clearPreviousFilteredPatients(){
        this.state = {
            ...this.state,
            filteredPatients: []
        };
        this.emit("change", this.state);
    }

    clearFilters(){
        this.state = {
            ...this.state,
            filterTag: "",
            filteredPatients: []
        };
        this.emit("change", this.state);
    }

    // --------------------------------------------------------
    // update
    changeUpdatedPatientProperty(property, value) {
        this.state = {
            ...this.state,
            updatedPatient: {
                ...this.state.updatedPatient,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    updatePatient(id, firstName, lastName, birthdate, gender, address, record){
        return this.restPatient.updatePatient(id, firstName, lastName, birthdate, gender, address, record);
    }


    // --------------------------------------------------------
    // delete
    changeDeletePatientProperty(value){
        this.state = {
            ...this.state,
            deleteId: value
        };
        this.emit("change", this.state);
    }

    deletePatient(id){
        return this.restPatient.deletePatientById(id);
    }


    appendSpecificPatient(patient){
        this.state = {
            ...this.state,
            specificPatients: this.state.specificPatients.concat([patient])
        };
        this.emit("change", this.state);
    }

    loadCertainPatients(username){
        return this.restPatient.loadCertainPatients(username)
            .then(patients =>{
                this.state.specificPatients = [];
                patients.forEach(p => this.appendSpecificPatient(p));

                this.emit("change", this.state);
            });
    }


}


const patientModel = new PatientModel();


export default patientModel;
