import { EventEmitter } from "events";
import RestUser from "../rest/RestUser";

        
class UserModel extends EventEmitter {
    constructor() {
        super();
        this.restUser = new RestUser();
        this.state = {
            loggedUser:{
                username: "",
                password: "",
                role: ""
            }
        };
    }

    logUser(){
        return this.restUser.getUser(this.state.loggedUser.username, this.state.loggedUser.password)
            .then(user =>{
                this.state = {
                    ...this.state,
                    loggedUser: {
                        ...this.state.loggedUser,
                        role: user.role
                    }
                };
                this.emit("change", this.state);
            });
    }

    changeLoggedUserProperty(property, value) {
        this.state = {
            ...this.state,
            loggedUser: {
                ...this.state.loggedUser,
                [property]: value
            }
        };
        this.emit("change", this.state);
    }

    validateLoggedUser() {
        return !(this.state.loggedUser.role === "");
    }
}


const userModel = new UserModel();


export default userModel;
