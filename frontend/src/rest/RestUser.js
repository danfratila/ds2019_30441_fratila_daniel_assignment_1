const BASE_URL = "http://localhost:8080/spring-demo/user/";

export default class RestUser {
    getUser(username, password){
        return fetch(BASE_URL + username + "/" + password, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }
}
