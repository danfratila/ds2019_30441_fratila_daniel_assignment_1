const BASE_URL = "http://localhost:8080/spring-demo/patient";

export default class RestPatient {
    loadAllPatients() {
        return fetch(BASE_URL + "/all", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    createPatient(firstName, lastName, birthdate, gender, address, record){
        return fetch(BASE_URL + "/insert", {
            method: "POST",
            body: JSON.stringify({
                firstname: firstName,
                surname: lastName,
                birthdate: birthdate,
                gender: gender,
                address: address,
                record: record
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            return response.json()
        });
    }

    loadCertainPatientByName(name) {
        return fetch(BASE_URL + "/details/" + name, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    loadCertainPatients(username){
        return fetch(BASE_URL + "/details/specific/" + username, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    loadCertainPatientById(id){
        return fetch(BASE_URL + "/details/" + id, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    updatePatient(patientId, firstname, lastname, birthdate, gender, address, record){
        return fetch(BASE_URL + "/update/" + patientId, {
            method: "PUT",
            body: JSON.stringify({
                id: patientId,
                firstname: firstname,
                surname: lastname,
                birthdate: birthdate,
                gender: gender,
                address: address,
                record: record
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response => {
            //return response.json()
            });
    }

    deletePatientById(id){
        return fetch(BASE_URL + "/delete/" + id, {
            method: "DELETE",
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response => {
            //return response.json()}
        });
    }
}
