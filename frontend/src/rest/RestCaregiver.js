const BASE_URL = "http://localhost:8080/spring-demo/caregiver";

export default class RestCaregiver {
    loadAllCaregivers() {
        return fetch(BASE_URL + "/all", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    createCaregiver(firstName, lastName, birthdate, gender, address){
        return fetch(BASE_URL + "/insert", {
            method: "POST",
            body: JSON.stringify({
                firstname: firstName,
                surname: lastName,
                birthdate: birthdate,
                gender: gender,
                address: address
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            return response.json()
        });
    }

    loadCertainCaregiverByName(name) {
        return fetch(BASE_URL + "/details/" + name, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    loadCertainCaregiverById(id){
        return fetch(BASE_URL + "/details/" + id, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    updateCaregiver(caregiverId, firstname, lastname, birthdate, gender, address){
        return fetch(BASE_URL + "/update/" + caregiverId, {
            method: "PUT",
            body: JSON.stringify({
                id: caregiverId,
                firstname: firstname,
                surname: lastname,
                birthdate: birthdate,
                gender: gender,
                address: address
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        });
    }

    deleteCaregiverById(id){
        return fetch(BASE_URL + "/delete/" + id, {
            method: "DELETE",
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response => {
            //return response.json()}
        });
    }
}
