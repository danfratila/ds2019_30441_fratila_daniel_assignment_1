const BASE_URL = "http://localhost:8080/spring-demo/medication";


export default class RestMedication {

    loadAllMedications() {
        return fetch(BASE_URL + "/all", {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            return response.json()});
    }

    loadCertainMedicationByName(name) {
        return fetch(BASE_URL + "/details/" + name, {
            method: "GET",
            headers: {
                //"Authorization": this.authorization
            }
        }).then(response => {
            console.log(response);
            return response.json()});
    }

    createMedication(name, sideEffects, dosage){
        return fetch(BASE_URL + "/insert", {
            method: "POST",
            body: JSON.stringify({
                name: name,
                sideEffects: sideEffects,
                dosage: dosage
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            return response.json()
        });
    }

    updateMedication(name, sideEffects, dosage){
        return fetch(BASE_URL + "/update/" + name, {
            method: "PUT",
            body: JSON.stringify({
                name: name,
                sideEffects: sideEffects,
                dosage: dosage
            }),
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            // console.log(response.json());
           // return response.text();
        });
    }

    deleteMedication(name){
        return fetch(BASE_URL + "/delete/" + name, {
            method: "DELETE",
            headers: {
                //"Authorization": this.authorization,
                "Content-Type" : "application/json"
            }
        }).then(response =>{
            return response.json()
        });
    }
}
