import medicationModel from "../../model/medicationModel";

class UpdateMedicationPresenter {
    onUpdateMed(){
        medicationModel.updateMedication(
            medicationModel.state.updatedMedication.name,
            medicationModel.state.updatedMedication.sideEffects,
            medicationModel.state.updatedMedication.dosage)
            .then(() => {
                medicationModel.changeUpdatedMedicationProperty("name", "");
                medicationModel.changeUpdatedMedicationProperty("sideEffects", "");
                medicationModel.changeUpdatedMedicationProperty("dosage", "");
                medicationModel.loadMedications();
            });
    }

    onChange(property, value) {
        medicationModel.changeUpdatedMedicationProperty(property, value);
    }
}

const updateMedicationPresenter = new UpdateMedicationPresenter();

export default updateMedicationPresenter;
