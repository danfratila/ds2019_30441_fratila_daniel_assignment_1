import medicationModel from "../../model/medicationModel";


class CreateMedicationPresenter {
    onCreate() {
        medicationModel.addMedication(
            medicationModel.state.newMedication.name,
            medicationModel.state.newMedication.sideEffects,
            medicationModel.state.newMedication.dosage)
            .then(() => {
                medicationModel.changeNewMedicationProperty("name", "");
                medicationModel.changeNewMedicationProperty("sideEffects", "");
                medicationModel.changeNewMedicationProperty("dosage", "");
                medicationModel.loadMedications();
            });
    }

    onChange(property, value) {
        medicationModel.changeNewMedicationProperty(property, value);
    }
}

const createMedicationPresenter = new CreateMedicationPresenter();

export default createMedicationPresenter;
