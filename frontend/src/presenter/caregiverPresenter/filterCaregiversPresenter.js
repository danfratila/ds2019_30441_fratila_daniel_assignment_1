import caregiverModel from "../../model/caregiverModel";

class FilterCaregiversPresenter {
    onFilterByName(){
        caregiverModel.clearPreviousFilteredCaregivers();
        caregiverModel.filterByName();
    }

    onChangeFilter(value){
        caregiverModel.changeFilterProperty(value);
    }

    onClearFilters(){
        caregiverModel.clearFilters();
    }
}

const filterCaregiversPresenter = new FilterCaregiversPresenter();

export default filterCaregiversPresenter;
