import patientModel from "../../model/patientModel";

class UpdatePatientPresenter {
    onUpdatePatient(){
        patientModel.updatePatient(
            patientModel.state.updatedPatient.patientId,
            patientModel.state.updatedPatient.firstname,
            patientModel.state.updatedPatient.lastname,
            patientModel.state.updatedPatient.birthdate,
            patientModel.state.updatedPatient.gender,
            patientModel.state.updatedPatient.address,
            patientModel.state.updatedPatient.record)
            .then( () => {
                patientModel.changeUpdatedPatientProperty("patientId", "") ;
                patientModel.changeUpdatedPatientProperty("firstname", "") ;
                patientModel.changeUpdatedPatientProperty("lastname", "") ;
                patientModel.changeUpdatedPatientProperty("birthdate", "") ;
                patientModel.changeUpdatedPatientProperty("gender", "") ;
                patientModel.changeUpdatedPatientProperty("address", "") ;
                patientModel.changeUpdatedPatientProperty("record", "") ;
                patientModel.loadPatients();
            });
    }

    onChange(property, value) {
        patientModel.changeUpdatedPatientProperty(property, value);
    }
}

const updatePatientPresenter = new UpdatePatientPresenter();

export default updatePatientPresenter;
