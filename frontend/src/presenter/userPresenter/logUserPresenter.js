import userModel from "../../model/userModel";

class LogUserPresenter {
    onLogIn() {
        userModel.logUser()
            .then(() =>{
                window.location.assign("#/" + userModel.state.loggedUser.role + "/welcome");
            });
    }

    onChange(value, property){
        userModel.changeLoggedUserProperty(value, property);
    }

    onValidateLoggedUser(){
        // console.log(window.location.href.split("#/")[1]);
        if(userModel.validateLoggedUser() === false ||
            !window.location.href.split("#/")[1].startsWith(userModel.state.loggedUser.role, 0)) {
            window.location.assign("#/"); // poate fac o pagina de eroare
        }
    }
}

const logUserPresenter = new LogUserPresenter();

export default logUserPresenter;
