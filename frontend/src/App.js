import React from 'react';
import './App.css';

import { HashRouter, Switch, Route } from "react-router-dom";

import SmartPatients from './view/patientView/SmartPatients';
import SmartCaregivers from './view/caregiverView/SmartCaregivers';
import SmartMedications from './view/medicationView/SmartMedications';
import SmartUsers from './view/userView/SmartUsers';

import SmartDoctorWelcome from "./view/welcome/doctor/SmartDoctorWelcome";
import SmartCaregiverWelcome from "./view/welcome/caregiver/SmartCaregiverWelcome";

const App = () => (
  <div className="App">
    <HashRouter>
      <Switch>
        <Route exact={true} component={SmartMedications} path="/doctor/medications" />
        <Route exact={true} component={SmartPatients} path="/doctor/patients" />
        <Route exact={true} component={SmartCaregivers} path="/doctor/caregivers" />
        <Route exact={true} component={SmartUsers} path="/" />


        <Route exact={true} component={SmartDoctorWelcome} path="/doctor/welcome" />
          <Route exact={true} component={SmartDoctorWelcome} path="/patient/welcome" />
          <Route exact={true} component={SmartCaregiverWelcome} path="/caregiver/welcome" />

      </Switch>
    </HashRouter>
  </div>
);


export default App;
