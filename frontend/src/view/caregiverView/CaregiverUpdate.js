import React from "react";

const CaregiverUpdate = ({ id, firstname, lastname, birthDate, gender, caregiverAddress, onUpdateCaregiver, onChange }) => (
    <div>
        <h2>Update caregiver</h2>
        <div>
            <label>Id of the caregiver to be modified: </label>
            <input value={id}
                   onChange={ e => onChange("caregiverId", e.target.value) } />
            <br />

            <label>First name: </label>
            <input value={firstname}
                   onChange={ e => onChange("firstname", e.target.value) } />
            <br />

            <label>Last name: </label>
            <input value={lastname}
                   onChange={ e => onChange("lastname", e.target.value) } />
            <br />

            <label>New birth date: </label>
            <input value={birthDate}
                   onChange={ e => onChange("birthdate", e.target.value) } />
            <br />

            <label>New gender: </label>
            <input value={gender}
                   onChange={ e => onChange("gender", e.target.value) } />
            <br />

            <label>New caregiver address: </label>
            <input value={caregiverAddress}
                   onChange={ e => onChange("address", e.target.value) } />
            <br />

            <button onClick={onUpdateCaregiver} >Change!</button>
        </div>
    </div>
);

export default CaregiverUpdate;
