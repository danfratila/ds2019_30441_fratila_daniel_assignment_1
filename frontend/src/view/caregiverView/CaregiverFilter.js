import React from "react";

const CaregiverFilter = ({ title, filteredName, onChangeFilteredName, onFilter, onClearFilters}) => (
    <div>
        <h2>{ title || "Filter caregivers" }</h2>

        <label>Filter caregivers with following name:</label>
        &emsp;
        <input value={filteredName}
               onChange={e => onChangeFilteredName(e.target.value)}/>
        <br/>
        <button className="btn btn-primary" onClick={onFilter}>Filter!</button>
        <button className="btn btn-primary" onClick={onClearFilters}>Clear filters</button>

    </div>
);

export default CaregiverFilter;
