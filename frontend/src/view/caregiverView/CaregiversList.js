import React from "react";

const CaregiversList = ({ caregivers, title}) => (
    <div>
        <h2>{ title || "Caregivers" }</h2>
        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>Birth date</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Id of patients</th>
                </tr>
            </thead>
            <tbody>
                {
                    caregivers.map((caregiver, index) => (
                        <tr key={index} >
                            <td>{caregiver.id}</td>
                            <td>{caregiver.firstname}</td>
                            <td>{caregiver.surname}</td>
                            <td>{caregiver.birthdate}</td>
                            <td>{caregiver.gender}</td>
                            <td>{caregiver.address}</td>
                            <td>{caregiver.patients}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    </div>
);

export default CaregiversList;
