import React from "react";

const DoctorWelcome = ({ onGoToPatients, onGoToCaregivers, onGoToMedications}) => (
    <div>
        <h2>View or modify:</h2>
        <div>
            <button onClick={onGoToPatients}>patients</button>
            <br/>
            <button onClick={onGoToCaregivers}>caregivers</button>
            <br/>
            <button onClick={onGoToMedications}>medications</button>
        </div>
    </div>
);

export default DoctorWelcome;
