import React, { Component } from "react";

import userModel from "../../../model/userModel";
import logUserPresenter from "../../../presenter/userPresenter/logUserPresenter";
import welcomeDoctorPresenter from "../../../presenter/welcomePresenter/welcomeDoctorPresenter";
import DoctorWelcome from "./DoctorWelcome";


const mapModelStateToComponentState = modelState => ({

});

export default class SmartDoctorWelcome extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(userModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        userModel.addListener("change", this.listener);
    }

    componentWillUnmount() {
        userModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                <DoctorWelcome
                    onGoToPatients={welcomeDoctorPresenter.onGoToPatients}
                    onGoToCaregivers={welcomeDoctorPresenter.onGoToCaregivers}
                    onGoToMedications={welcomeDoctorPresenter.onGoToMedications}
                />
            </div>
        );
    }
}
