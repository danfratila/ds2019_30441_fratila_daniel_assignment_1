import React, { Component } from "react";

import patientModel from "../../../model/patientModel";
import logUserPresenter from "../../../presenter/userPresenter/logUserPresenter";
import listPatientsPresenter from "../../../presenter/patientPresenter/listPatientsPresenter";
import PatientsList from "../../patientView/PatientsList";


const mapModelStateToComponentState = modelState => ({
    patients: modelState.specificPatients
});

export default class SmartCaregiverWelcome extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(patientModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        patientModel.addListener("change", this.listener);
    }

    componentWillUnmount() {
        patientModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                <PatientsList
                    patients={this.state.patients}
                    refresh={listPatientsPresenter.onLoadCertainPatients}
                />

            </div>
        );
    }
}
