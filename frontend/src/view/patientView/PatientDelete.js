import React from "react";

const PatientDelete = ({ deleteId, onChangeDeleteId, onDelete}) => (
    <div>
        <h2>{"Delete patients"}</h2>
        <label>Delete the patient with following id:</label>
        &emsp;
        <input value={deleteId}
               onChange={e => onChangeDeleteId(e.target.value)}/>
        <br/>
        <button className="btn btn-primary" onClick={onDelete}>Delete!</button>

    </div>
);

export default PatientDelete;
