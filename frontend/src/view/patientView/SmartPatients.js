import React, { Component } from "react";
import patientModel from "../../model/patientModel";


import PatientCreate from "../patientView/PatientCreate";
import PatientDelete from "../patientView/PatientDelete";
import PatientFilter from "../patientView/PatientFilter";
import PatientUpdate from "../patientView/PatientUpdate";
import PatientsList from "../patientView/PatientsList";

import createPatientPresenter from "../../presenter/patientPresenter/createPatientPresenter"
import deletePatientPresenter from "../../presenter/patientPresenter/deletePatientPresenter"
import filterPatientsPresenter from "../../presenter/patientPresenter/filterPatientsPresenter"
import listPatientsPresenter from "../../presenter/patientPresenter/listPatientsPresenter"
import updatePatientPresenter from "../../presenter/patientPresenter/updatePatientPresenter"
import logUserPresenter from  "../../presenter/userPresenter/logUserPresenter"

const mapModelStateToComponentState = modelState => ({
    patients: modelState.patients,
    filteredPatients: modelState.filteredPatients,

    newFirstname: modelState.newPatient.firstname,
    newLastname: modelState.newPatient.lastname,
    newBirthdate: modelState.newPatient.birthdate,
    newGender: modelState.newPatient.gender,
    newAddress: modelState.newPatient.address,
    newRecord: modelState.newPatient.record,

    updatedId: modelState.updatedPatient.patientId,
    updatedFirstname: modelState.updatedPatient.firstname,
    updatedLastname: modelState.updatedPatient.lastname,
    updatedBirthdate: modelState.updatedPatient.birthdate,
    updatedGender: modelState.updatedPatient.gender,
    updatedAddress: modelState.updatedPatient.address,
    updatedRecord: modelState.updatedPatient.record,

    filterTag: modelState.filterTag,
    deleteId: modelState.deleteId
});

export default class SmartPatients extends Component {
    constructor() {
        super();
        this.state = mapModelStateToComponentState(patientModel.state);
        this.listener = modelState => this.setState(mapModelStateToComponentState(modelState));
        logUserPresenter.onValidateLoggedUser();
        patientModel.addListener("change", this.listener);
        listPatientsPresenter.onInit();
    }

    componentWillUnmount() {
        patientModel.removeListener("change", this.listener);
    }

    render() {
        return (
            <div>
                {/*Validate user every time a page is loaded*/}
                {logUserPresenter.onValidateLoggedUser()}

                {/*Create*/}
                <PatientCreate
                    firstname={this.state.newFirstname}
                    lastname={this.state.newLastname}
                    birthDate={this.state.newBirthdate}
                    gender={this.state.newGender}
                    patientAddress={this.state.newAddress}
                    record={this.state.newRecord}
                    onChange={createPatientPresenter.onChange}
                    onCreate={createPatientPresenter.onCreate}
                />

                {/*Read all*/}
                <PatientsList
                    patients={this.state.patients}
                />

                {/*Filter (Search by first name)*/}
                <PatientFilter
                    filteredName={this.state.filterTag}
                    onChangeFilteredName={filterPatientsPresenter.onChangeFilter}
                    onFilter={filterPatientsPresenter.onFilterByName}
                    onClearFilters={filterPatientsPresenter.onClearFilters}
                />

                {/*Read filtered*/}
                <PatientsList
                    title={"Filtered patients"}
                    patients={this.state.filteredPatients}
                />

                {/*Update*/}
                <PatientUpdate
                    id={this.state.updatedId}
                    firstname={this.state.updatedFirstname}
                    lastname={this.state.updatedLastname}
                    birthDate={this.state.updatedBirthdate}
                    gender={this.state.updatedGender}
                    patientAddress={this.state.updatedAddress}
                    record={this.state.updatedRecord}
                    onUpdatePatient={updatePatientPresenter.onUpdatePatient}
                    onChange={updatePatientPresenter.onChange}
                />

                {/*Delete*/}
                <PatientDelete
                    deleteId={this.state.deleteId}
                    onChangeDeleteId={deletePatientPresenter.onChangeDeleteId}
                    onDelete={deletePatientPresenter.onDelete}
                />
            </div>
        );
    }
}
