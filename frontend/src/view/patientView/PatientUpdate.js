import React from "react";

const PatientUpdate = ({ id, firstname, lastname, birthDate, gender, patientAddress, record, onUpdatePatient, onChange }) => (
    <div>
        <h2>Update patient</h2>
        <div>
            <label>Id of the patient to be modified: </label>
            <input value={id}
                   onChange={ e => onChange("patientId", e.target.value) } />
            <br />

            <label>First name: </label>
            <input value={firstname}
                   onChange={ e => onChange("firstname", e.target.value) } />
            <br />

            <label>Last name: </label>
            <input value={lastname}
                   onChange={ e => onChange("lastname", e.target.value) } />
            <br />

            <label>New birth date: </label>
            <input value={birthDate}
                   onChange={ e => onChange("birthdate", e.target.value) } />
            <br />

            <label>New gender: </label>
            <input value={gender}
                   onChange={ e => onChange("gender", e.target.value) } />
            <br />

            <label>New patient address: </label>
            <input value={patientAddress}
                   onChange={ e => onChange("address", e.target.value) } />
            <br />

            <label>New medical record: </label>
            <input value={record}
                   onChange={ e => onChange("record", e.target.value) } />
            <br />

            <button onClick={onUpdatePatient} >Change!</button>
        </div>
    </div>
);

export default PatientUpdate;
