package spring.demo.dto;

import java.util.Date;

public class PatientDTO {

	private Integer id;
	private String firstname;
	private String surname;
	private String birthdate;
	private String gender;
	private String address;
	private String record;
	

	public PatientDTO() {
	}

	public PatientDTO(Integer id, String firstname, String surname, String birthdate, String gender, String address, String record) {
		super();
		this.id = id;
		this.firstname= firstname;
		this.surname = surname;
		this.birthdate = birthdate;
		this.gender = gender;
		this.address = address;
		this.record = record;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRecord() {
		return this.record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public static class Builder {
		private Integer nestedid;
		private String nestedfirstname;
		private String nestedsurname;
		private String nestedbirthdate;
		private String nestedgender;
		private String nestedaddress;
		private String nestedrecord;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder firstname(String name) {
			this.nestedfirstname = name;
			return this;
		}
		
		public Builder surname(String name) {
			this.nestedsurname = name;
			return this;
		}

		public Builder birthdate(Date date) {
			this.nestedbirthdate = date.toString();
			return this;
		}
		
		public Builder gender(String gender) {
			this.nestedgender = gender;
			return this;
		}
		
		public Builder address(String address) {
			this.nestedaddress = address;
			return this;
		}
		
		public Builder record(String record) {
			this.nestedrecord = record;
			return this;
		}

		public PatientDTO create() {
			return new PatientDTO(nestedid, nestedfirstname, nestedsurname, nestedbirthdate, nestedgender, nestedaddress, nestedrecord);
		}

	}

}
