package spring.demo.dto;

import java.util.Date;

public class UserDTO {

	private Integer id;
	private String firstname;
	private String surname;
	private String birthdate;
	private String gender;
	private String address;
	private String username;
	private String password;
	private String role;
	
	

	public UserDTO() {
	}

	public UserDTO(Integer id, String firstname, String surname, String birthdate, String gender, String address) {
		super();
		this.id = id;
		this.firstname= firstname;
		this.surname = surname;
		this.birthdate = birthdate;
		this.gender = gender;
		this.address = address;
	}
	
	public UserDTO(Integer id, String firstname, String surname, String birthdate, String gender, String address, String username, String password, String role) {
		super();
		this.id = id;
		this.firstname= firstname;
		this.surname = surname;
		this.birthdate = birthdate;
		this.gender = gender;
		this.address = address;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}



	public static class Builder {
		private Integer nestedid;
		private String nestedfirstname;
		private String nestedsurname;
		private String nestedbirthdate;
		private String nestedgender;
		private String nestedaddress;
		private String nestedusername;
		private String nestedpassword;
		private String nestedrole;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder firstname(String name) {
			this.nestedfirstname = name;
			return this;
		}
		
		public Builder surname(String name) {
			this.nestedsurname = name;
			return this;
		}

		public Builder birthdate(Date date) {
			this.nestedbirthdate = date.toString();
			return this;
		}
		
		public Builder gender(String gender) {
			this.nestedgender = gender;
			return this;
		}
		
		public Builder address(String address) {
			this.nestedaddress = address;
			return this;
		}		
		
		public Builder username(String username) {
			this.nestedusername = username;
			return this;
		}	
		
		public Builder password(String password) {
			this.nestedpassword= password;
			return this;
		}	
		
		public Builder role(String role) {
			this.nestedrole= role;
			return this;
		}

//		public UserDTO create() {
//			return new UserDTO(nestedid, nestedfirstname, nestedsurname, nestedbirthdate, nestedgender, nestedaddress);
//		}

		public UserDTO create() {
			return new UserDTO(nestedid, nestedfirstname, nestedsurname, nestedbirthdate, nestedgender, nestedaddress, nestedusername, nestedpassword, nestedrole);
		}
		
	}

}
