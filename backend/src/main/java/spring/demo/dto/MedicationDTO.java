package spring.demo.dto;

public class MedicationDTO {

	private Integer id;
	private String name;
	private String sideEffects;
	private Integer dosage;

	public MedicationDTO() {
	}

	public MedicationDTO(Integer id, String name, String sideEffects, Integer dosage) {
		super();
		this.id = id;
		this.name = name;
		this.sideEffects = sideEffects;
		this.dosage = dosage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(String effects) {
		this.sideEffects = effects;
	}
	
	public Integer getDosage() {
		return dosage;
	}

	public void setDosage(Integer dosage) {
		this.dosage = dosage;
	}



	public static class Builder {
		private Integer nestedid;
		private String nestedname;
		private String nestedsideeffects;
		private Integer nesteddosage;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder name(String name) {
			this.nestedname = name;
			return this;
		}
		
		public Builder sideEffects(String sideEffects) {
			this.nestedsideeffects = sideEffects;
			return this;
		}
		
		public Builder dosage(Integer dosage) {
			this.nesteddosage = dosage;
			return this;
		}

		public MedicationDTO create() {
			return new MedicationDTO(nestedid, nestedname, nestedsideeffects, nesteddosage);
		}

	}

}
