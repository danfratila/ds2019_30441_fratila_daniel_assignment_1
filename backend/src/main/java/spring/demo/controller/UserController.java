package spring.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.UserDTO;
import spring.demo.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	// read
	@RequestMapping(value = "/{username}/{password}", method = RequestMethod.GET)
	public UserDTO getSpecificUser(@PathVariable("username") String username, @PathVariable("password") String password) {
		return userService.getSpecifiedUser(username, password);
	}
}

