package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.PatientDTO;
import spring.demo.services.PatientService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/patient")
public class PatientController {

	@Autowired
	private PatientService patientService;

	// create -- ok
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertPatient(@RequestBody PatientDTO patientDTO) {
		return patientService.create(patientDTO);
	}
	
	// read - filter by name -- ok
	@RequestMapping(value = "/details/{name}", method = RequestMethod.GET)
	public List<PatientDTO> getPatientByName(@PathVariable("name") String name) {
		return patientService.findPatientByName(name);
	}

	@RequestMapping(value = "/details/specific/{username}", method = RequestMethod.GET)
	public List<PatientDTO> getSpecificPatients(@PathVariable("username") String username) {
		return patientService.findSpecificPatients(username);
	}
	
	// read all -- ok
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<PatientDTO> getAllPatients() {
		return patientService.findAll();
	}

	// update -- ok
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public String updatePatient(@PathVariable("id") Integer id, @RequestBody PatientDTO patientDTO) {		
		patientService.update(id, patientDTO);
		return "verde";
	}
	
	// delete -- ok
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public PatientDTO deletePatient(@PathVariable("id") Integer id) {
		return patientService.delete(id);
	}
}

