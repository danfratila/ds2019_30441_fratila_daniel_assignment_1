package spring.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import spring.demo.dto.DoctorDTO;
import spring.demo.services.DoctorService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/doctor")
public class DoctorController {

	@Autowired
	private DoctorService doctorService;

	// create
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public int insertDoctor(@RequestBody DoctorDTO doctorDTO) {
		return doctorService.create(doctorDTO);
	}
	
	// read
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public DoctorDTO getDoctorById(@PathVariable("id") int id) {
		return doctorService.findDoctorById(id);
	}

	// read
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<DoctorDTO> getAllDoctors() {
		return doctorService.findAll();
	}

	// update
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	
	// delete
	//@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
}

