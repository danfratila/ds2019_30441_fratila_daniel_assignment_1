package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "patients")
public class Patient implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String medicalRecord;

	public Patient() {
	}

	public Patient(Integer id, String medicalRecord) {
		super();
		this.id = id;
		this.medicalRecord = medicalRecord;
	}

	@Id
	@Column(name = "id")
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "record")
	public String getMedicalRecord() {
		return this.medicalRecord;
	}
	
	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}
	
}
