package spring.demo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "caregivers")
public class Caregiver implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String patients;

	public Caregiver() {
	}

	public Caregiver(Integer id) {
		super();
		this.id = id;
		this.patients = "";
	}

	public Caregiver(Integer id, String patients) {
		super();
		this.id = id;
		this.patients = patients;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "patients")
	public String getPatients() {
		return this.patients;
	}

	public void setPatients(String patients) {
		this.patients = patients;
	}
}
