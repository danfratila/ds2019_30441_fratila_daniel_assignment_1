package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Integer> {

	Medication findByName(String name);

	Medication findById(int id);

}
