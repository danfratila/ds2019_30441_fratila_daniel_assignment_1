package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

	Patient findById(int id);
	
}
