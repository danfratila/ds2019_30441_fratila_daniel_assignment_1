package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

	Doctor findById(int id);

}
