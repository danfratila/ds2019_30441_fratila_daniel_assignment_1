package spring.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import spring.demo.entities.Caregiver;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

	Caregiver findById(int id);

}
