package spring.demo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.ResourceNotFoundException;

import spring.demo.entities.User;
import spring.demo.repositories.UserRepository;

import spring.demo.dto.CaregiverDTO;
import spring.demo.entities.Caregiver;
import spring.demo.repositories.CaregiverRepository;


@Service
public class CaregiverService {
	private static final String SPLIT_CH = " ";

	@Autowired
	private UserRepository usrRepository;
	
	@Autowired
	private CaregiverRepository caregiverRepository;

	public CaregiverDTO findCaregiverById(int id) {
		User usr = usrRepository.findById(id);
		Caregiver caregiver = caregiverRepository.findById(id);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		else if(caregiver == null) {
			throw new ResourceNotFoundException(Caregiver.class.getSimpleName());
		}
		
		String[] names = extractNames(usr.getName());

		CaregiverDTO dto = new CaregiverDTO.Builder()
						.id(usr.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(usr.getBirthdate())
						.gender(usr.getGender())
						.address(usr.getAddress())
//						.record(caregiver.getMedicalRecord())
						.create();
		return dto;
	}
	
	public List<CaregiverDTO> findAll() {
		List<Caregiver> caregivers = caregiverRepository.findAll();
		List<CaregiverDTO> toReturn = new ArrayList<CaregiverDTO>();
		for (Caregiver caregiver: caregivers) {
			User user = usrRepository.findById(caregiver.getId());
			String[] names = extractNames(user.getName());
			CaregiverDTO dto = new CaregiverDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
						.patients(caregiverRepository.findById(user.getId()).getPatients())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(CaregiverDTO caregiverDTO) {
		User user = new User();
		
		user.setName(caregiverDTO.getFirstname() + " " + caregiverDTO.getSurname());
		Date date = new Date();
		try {
			System.out.println(date);
			date = new SimpleDateFormat("yyyy-MM-dd").parse(caregiverDTO.getBirthdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		user.setBirthdate(date);
		user.setGender(caregiverDTO.getGender());
		user.setAddress(caregiverDTO.getAddress());
		
		User newUser = usrRepository.save(user);
		
		Caregiver caregiver = new Caregiver();
		caregiver.setId(newUser.getId());
//		caregiver.setMedicalRecord(caregiverDTO.getRecord());
		
		Caregiver ptnt = caregiverRepository.save(caregiver);
		return ptnt.getId();
	}

	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}

	public List<CaregiverDTO> findCaregiverByName(String name) {
		// TODO Auto-generated method stub
		List<CaregiverDTO> dtos = new ArrayList<CaregiverDTO>();
		List<User> users = usrRepository.findAll(); 
		for(User user: users) {
			if(caregiverRepository.findById(user.getId()) != null && user.getName().toLowerCase().contains(name.toLowerCase())) {
				String[] names = extractNames(user.getName());
				CaregiverDTO dto = new CaregiverDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
//						.record(caregiverRepository.findById(user.getId()).getMedicalRecord())
						.create();
				dtos.add(dto);
			}
		}
		return dtos;
	}

	public void update(Integer id, CaregiverDTO caregiverDTO) {
		// TODO Auto-generated method stub
		User user = usrRepository.findById(id); // normally same with caregiver dto
		if(user != null) {
			String[] names = extractNames(user.getName());
			Caregiver caregiver = caregiverRepository.findById(user.getId());
			if(caregiverDTO.getFirstname() != null && caregiverDTO.getFirstname() != "") {
				names[0] = caregiverDTO.getFirstname();
			}
			if(caregiverDTO.getSurname() != null && caregiverDTO.getSurname() != "") {
				names[1] = caregiverDTO.getSurname();
			}
			user.setName(names[0] + " " + names[1]);
			if(caregiverDTO.getBirthdate() != null && caregiverDTO.getBirthdate() != "") {
				Date date = new Date();
				try {
					System.out.println(date);
					date = new SimpleDateFormat("yyyy-MM-dd").parse(caregiverDTO.getBirthdate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				user.setBirthdate(date);
			}
			if(caregiverDTO.getGender() != null && caregiverDTO.getGender() != "") {
				user.setGender(caregiverDTO.getGender());
			}
			if(caregiverDTO.getAddress() != null && caregiverDTO.getAddress() != "") {
				user.setAddress(caregiverDTO.getAddress());
			}
			usrRepository.save(user);

			
//			if(caregiverDTO.getRecord() != null && caregiverDTO.getRecord() != "") {
//				caregiver.setMedicalRecord(caregiverDTO.getRecord());
//				caregiverRepository.save(caregiver);
//			}
		}
	}

	public CaregiverDTO delete(Integer id) {
		User user = usrRepository.findById(id);
		if(user != null) {
			usrRepository.delete(user);
			caregiverRepository.delete(caregiverRepository.findById(id));
		}
		return new CaregiverDTO();
	}
}
