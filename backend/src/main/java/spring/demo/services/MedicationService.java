package spring.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.ResourceNotFoundException;

import spring.demo.dto.MedicationDTO;
import spring.demo.entities.Medication;
import spring.demo.repositories.MedicationRepository;

@Service
public class MedicationService {

	@Autowired
	private MedicationRepository medicationRepository;

	public MedicationDTO findMedicationById(int id) {
		Medication med = medicationRepository.findById(id);
		if (med == null) {
			throw new ResourceNotFoundException(Medication.class.getSimpleName());
		}

		MedicationDTO dto = new MedicationDTO.Builder()
						.id(med.getId())
						.name(med.getName())
						.sideEffects(med.getSideEffects())
						.dosage(med.getDosage())
						.create();
		return dto;
	}
	
	public List<MedicationDTO> findMedicationByName(String name) {
		List<MedicationDTO> returnDtos = new ArrayList<MedicationDTO>();
		List<Medication> meds = medicationRepository.findAll();
		for(Medication med: meds) {
			if(med.getName().toLowerCase().contains(name.toLowerCase())) {
				MedicationDTO dto = new MedicationDTO.Builder()
						.id(med.getId())
						.name(med.getName())
						.sideEffects(med.getSideEffects())
						.dosage(med.getDosage())
						.create();
				returnDtos.add(dto);
			}
		}
		return returnDtos;
	}
	
	public List<MedicationDTO> findAll() {
		List<Medication> meds = medicationRepository.findAll();
		List<MedicationDTO> toReturn = new ArrayList<MedicationDTO>();
		for (Medication med: meds) {
			MedicationDTO dto = new MedicationDTO.Builder()
						.id(med.getId())
						.name(med.getName())
						.sideEffects(med.getSideEffects())
						.dosage(med.getDosage())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(MedicationDTO medDTO) {
		Medication medication = new Medication();
		
		medication.setName(medDTO.getName());
		medication.setSideEffects(medDTO.getSideEffects());
		medication.setDosage(medDTO.getDosage());
		
		Medication med = medicationRepository.save(medication);
		return med.getId();
	}
	

	public void update(String name, MedicationDTO medDTO) {
		Medication med = medicationRepository.findByName(name);
		if(med != null) {
			med.setName(medDTO.getName()); // is the same
			if(medDTO.getSideEffects() != null && medDTO.getSideEffects() != "") {
				med.setSideEffects(medDTO.getSideEffects()); 	
			}
			if(medDTO.getDosage() != null) {
				med.setDosage(medDTO.getDosage());
			}
			medDTO.setId(med.getId());
			medicationRepository.save(med);
		}
		//return medDTO;
	}
	
	
	public MedicationDTO delete(String name) {
		Medication med = medicationRepository.findByName(name);
		if(med != null) {
			medicationRepository.delete(med);
		}
		return new MedicationDTO();
	}
	

}
