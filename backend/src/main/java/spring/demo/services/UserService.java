package spring.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.dto.UserDTO;
import spring.demo.entities.User;
import spring.demo.repositories.CaregiverRepository;
import spring.demo.repositories.DoctorRepository;
import spring.demo.repositories.PatientRepository;
import spring.demo.repositories.UserRepository;

@Service
public class UserService {
	private static final String SPLIT_CH = " ";

	@Autowired
	private UserRepository usrRepository;
	@Autowired
	private PatientRepository patientRepository;
	@Autowired
	private DoctorRepository doctorRepository;
	@Autowired
	private CaregiverRepository caregiverRepository;

	public UserDTO getSpecifiedUser(String username, String password) {
		User usr = usrRepository.findByUsername(username);
		UserDTO dto = new UserDTO();
		if (usr != null) {
			dto = new UserDTO.Builder()
					.role(usr.getRole())
					.create();
		}
		System.out.println("Rol::" + dto.getRole());
		return dto;
	}
}
