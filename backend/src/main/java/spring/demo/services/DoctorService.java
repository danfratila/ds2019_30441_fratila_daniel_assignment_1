package spring.demo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.EntityValidationException;
import spring.demo.errorhandler.ResourceNotFoundException;


import spring.demo.entities.User;
import spring.demo.repositories.UserRepository;

import spring.demo.entities.Doctor;
import spring.demo.dto.DoctorDTO;
import spring.demo.repositories.DoctorRepository;

@Service
public class DoctorService {
	private static final String SPLIT_CH = " ";
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	@Autowired
	private DoctorRepository doctorRepository;

	@Autowired
	private UserRepository userRepository;
	
	public DoctorDTO findDoctorById(int id) {
		User usr = userRepository.findById(id);
		Doctor patient = doctorRepository.findById(id);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		else if(patient == null) {
			throw new ResourceNotFoundException(Doctor.class.getSimpleName());
		}
		
		String[] names = extractNames(usr.getName());

		DoctorDTO dto = new DoctorDTO.Builder()
						.id(usr.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(usr.getBirthdate())
						.gender(usr.getGender())
						.address(usr.getAddress())
						.create();
		return dto;
	}
	
	public List<DoctorDTO> findAll() {
		List<Doctor> doctors = doctorRepository.findAll();
		List<DoctorDTO> toReturn = new ArrayList<DoctorDTO>();
		for (Doctor doctor: doctors) {
			User user = userRepository.findById(doctor.getId());
			String[] names = extractNames(user.getName());
			DoctorDTO dto = new DoctorDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(DoctorDTO doctorDTO) {
		// List<String> validationErrors = validateUser(userDTO);
//		if (!validationErrors.isEmpty()) {
//			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
//		}

		Doctor doctor = new Doctor();
		
		Doctor doc = doctorRepository.save(doctor);
		return doc.getId();
	}

//	private List<String> validateUser(UserDTO usr) {
//		List<String> validationErrors = new ArrayList<String>();
//
//		if (usr.getFirstname() == null || "".equals(usr.getFirstname())) {
//			validationErrors.add("First Name field should not be empty");
//		}
//
//		if (usr.getSurname() == null || "".equals(usr.getSurname())) {
//			validationErrors.add("Surname field should not be empty");
//		}
//
//		return validationErrors;
//	}

	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}
}
