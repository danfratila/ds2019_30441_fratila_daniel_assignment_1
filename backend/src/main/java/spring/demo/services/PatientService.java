package spring.demo.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.demo.errorhandler.ResourceNotFoundException;

import spring.demo.entities.User;
import spring.demo.repositories.UserRepository;
import spring.demo.dto.PatientDTO;
import spring.demo.entities.Patient;
import spring.demo.repositories.CaregiverRepository;
import spring.demo.repositories.PatientRepository;


@Service
public class PatientService {
	private static final String SPLIT_CH = " ";

	@Autowired
	private UserRepository usrRepository;
	
	@Autowired
	private PatientRepository patientRepository;	
	
	@Autowired
	private CaregiverRepository caregiverRepository;

	public PatientDTO findPatientById(int id) {
		User usr = usrRepository.findById(id);
		Patient patient = patientRepository.findById(id);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}
		else if(patient == null) {
			throw new ResourceNotFoundException(Patient.class.getSimpleName());
		}
		
		String[] names = extractNames(usr.getName());

		PatientDTO dto = new PatientDTO.Builder()
						.id(usr.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(usr.getBirthdate())
						.gender(usr.getGender())
						.address(usr.getAddress())
						.record(patient.getMedicalRecord())
						.create();
		return dto;
	}
	
	public List<PatientDTO> findAll() {
		List<Patient> patients = patientRepository.findAll();
		List<PatientDTO> toReturn = new ArrayList<PatientDTO>();
		for (Patient patient: patients) {
			User user = usrRepository.findById(patient.getId());
			String[] names = extractNames(user.getName());
			PatientDTO dto = new PatientDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
						.record(patient.getMedicalRecord())
						.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(PatientDTO patientDTO) {
		User user = new User();
		
		user.setName(patientDTO.getFirstname() + " " + patientDTO.getSurname());
		Date date = new Date();
		try {
			System.out.println(date);
			date = new SimpleDateFormat("yyyy-MM-dd").parse(patientDTO.getBirthdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		user.setBirthdate(date);
		user.setGender(patientDTO.getGender());
		user.setAddress(patientDTO.getAddress());
		
		User newUser = usrRepository.save(user);
		
		Patient patient = new Patient();
		patient.setId(newUser.getId());
		patient.setMedicalRecord(patientDTO.getRecord());
		
		Patient ptnt = patientRepository.save(patient);
		return ptnt.getId();
	}

	private String[] extractNames(String fullname){
		String[] names = new String[2];
		int surnameIndex = fullname.lastIndexOf(SPLIT_CH);
		names[0] = fullname;
		names[1] = "";
		if (surnameIndex != -1) {
			names[0] = fullname.substring(0, surnameIndex).trim();
			names[1] = fullname.substring(surnameIndex).trim();
		}
		return names;
	}

	public List<PatientDTO> findPatientByName(String name) {
		// TODO Auto-generated method stub
		List<PatientDTO> dtos = new ArrayList<PatientDTO>();
		List<User> users = usrRepository.findAll(); 
		for(User user: users) {
			if(patientRepository.findById(user.getId()) != null && user.getName().toLowerCase().contains(name.toLowerCase())) {
				String[] names = extractNames(user.getName());
				PatientDTO dto = new PatientDTO.Builder()
						.id(user.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(user.getBirthdate())
						.gender(user.getGender())
						.address(user.getAddress())
						.record(patientRepository.findById(user.getId()).getMedicalRecord())
						.create();
				dtos.add(dto);
			}
		}
		return dtos;
	}

	public void update(Integer id, PatientDTO patientDTO) {
		// TODO Auto-generated method stub
		User user = usrRepository.findById(id); // normally same with patient dto
		if(user != null) {
			String[] names = extractNames(user.getName());
			Patient patient = patientRepository.findById(user.getId());
			if(patientDTO.getFirstname() != null && patientDTO.getFirstname() != "") {
				names[0] = patientDTO.getFirstname();
			}
			if(patientDTO.getSurname() != null && patientDTO.getSurname() != "") {
				names[1] = patientDTO.getSurname();
			}
			user.setName(names[0] + " " + names[1]);
			if(patientDTO.getBirthdate() != null && patientDTO.getBirthdate() != "") {
				Date date = new Date();
				try {
					System.out.println(date);
					date = new SimpleDateFormat("yyyy-MM-dd").parse(patientDTO.getBirthdate());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				user.setBirthdate(date);
			}
			if(patientDTO.getGender() != null && patientDTO.getGender() != "") {
				user.setGender(patientDTO.getGender());
			}
			if(patientDTO.getAddress() != null && patientDTO.getAddress() != "") {
				user.setAddress(patientDTO.getAddress());
			}
			usrRepository.save(user);

			
			if(patientDTO.getRecord() != null && patientDTO.getRecord() != "") {
				patient.setMedicalRecord(patientDTO.getRecord());
				patientRepository.save(patient);
			}
		}
	}

	public PatientDTO delete(Integer id) {
		User user = usrRepository.findById(id);
		if(user != null) {
			usrRepository.delete(user);
			patientRepository.delete(patientRepository.findById(id));
		}
		return new PatientDTO();
	}
	
	private List<Integer> extractIds(String listOfIds){
		List<Integer> ids = new ArrayList<Integer>();
		String[] myList = listOfIds.split(",");
		
		for(String s: myList) {
			ids.add(Integer.parseInt(s.trim()));
		}
		
		return ids;
	}

	public List<PatientDTO> findSpecificPatients(String username) {
		// TODO Auto-generated method stub
		User requesterUser = usrRepository.findByUsername(username);
		String listOfIds = caregiverRepository.findById(requesterUser.getId()).getPatients();
		List<PatientDTO> dtos = new ArrayList<PatientDTO>();
		List<Integer> ids = extractIds(listOfIds);
		for(Integer i: ids) {
			User u = usrRepository.findById(i);
			Patient p = patientRepository.findById(i);
			if(u != null && p != null) {
				String[] names = extractNames(u.getName());
				PatientDTO dto = new PatientDTO.Builder()
						.id(u.getId())
						.firstname(names[0])
						.surname(names[1])
						.birthdate(u.getBirthdate())
						.gender(u.getGender())
						.address(u.getAddress())
						.record(p.getMedicalRecord())
						.create();
				dtos.add(dto);
			}
		}
		return dtos;
	}
}
